package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // Game loop
        while(true) {
            // Round counter
            System.out.println("Let's play round " + roundCounter);
            
            // Human choice
            String humanChoice = userChoice();

            String computerChoice = randomChoice();
            String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);

            // While loop for determening winners
            if(humanChoice.equals(computerChoice)) {
                System.out.println(choiceString + " It's a tie");
            }
            else if (humanChoice.equals("rock")) {
                if (computerChoice.equals("paper")) {
                    System.out.println(choiceString + " Computer wins.");
                    computerScore ++;
                } 
                    else if (computerChoice.equals("scissors")) {
                        System.out.println(choiceString + " Human wins.");
                        humanScore ++;
                    }
            }

            else if (humanChoice.equals("paper")) {
                if (computerChoice.equals("rock")) {
                    System.out.println(choiceString + " Human wins.");
                    humanScore ++;
                } 
                    else if (computerChoice.equals("scissors")) {
                        System.out.println(choiceString + " Computer wins.");
                        computerScore ++;
                    }
            }
            else if (humanChoice.equals("scissors")) {
                if (computerChoice.equals("paper")) {
                    System.out.println(choiceString + " Human wins.");
                    humanScore ++;
                } 
                    else if (computerChoice.equals("rock")) {
                        System.out.println(choiceString + " Computer wins.");
                        computerScore ++;
                    }
            }


            // Score
            String score = String.format("Score: human %s, computer %s", humanScore, computerScore);
            System.out.println(score);

            // Aks if human wants to play again
            String continueAnswer = continuePlaying();
            if(continueAnswer.equals("n")) {
                break;
            }
            // Round counter increases
            roundCounter ++;
        }
        // once player inputs "n" the while loop breaks and the system prints out "bye bye:)"
        System.out.println("Bye bye :)");
    }

    // Gets the computers choice
    public String randomChoice() {
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return randomElement;
    }

    public String userChoice() {
        while(true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
            if(humanChoice.equals("rock")) {
                return humanChoice;
            }
            else if(humanChoice.equals("paper")) {
                return humanChoice;
            }
            else if(humanChoice.equals("scissors")) {
                return humanChoice;
            }
            else {
                System.out.println(String.format("I do not understand %s. Could you try again?", humanChoice));
            }
        }
    }

    public String continuePlaying() {
        while(true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if(continueAnswer.equals("y")) {
                return continueAnswer;
            }
            else if(continueAnswer.equals("n")) {
                return continueAnswer;
            }
            else {
                System.out.println(String.format("I don't understand %s. Try again", continueAnswer));
            }
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
